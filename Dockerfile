FROM ruby:2.7.3

WORKDIR /app
COPY Gemfile Gemfile.lock ./

EXPOSE 8080:80

RUN bundle check || bundle install

COPY . ./

ENTRYPOINT ["./entrypoints/docker-entrypoint.sh"]
