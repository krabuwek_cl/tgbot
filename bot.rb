require 'telegram/bot'
# require 'pry'

TOKEN = "2040781182:AAFe8xr0eIF7YJxGYZhPTuaIf0MhxCJrMO4"
ADMIN_CHAT_ID = 455708564

Telegram::Bot::Client.run(TOKEN) do |bot|
  bot.listen do |message|
    case message.text
    when '/start'
      bot.api.send_message(chat_id: message.chat.id, text: "Hello, #{message.from.first_name}")
    when '/stop'
      bot.api.send_message(chat_id: message.chat.id, text: "Bye, #{message.from.first_name}")
    else
    	if !message.photo.first.nil?
    		bot.api.send_photo(chat_id: ADMIN_CHAT_ID, photo: message.photo.last.file_id, caption: message.caption)
    	elsif !message.video.nil?
    		bot.api.send_video(chat_id: ADMIN_CHAT_ID, video: message.video.file_id, caption: message.caption)
      end    	
    end
  end
end